import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {


  private urlapi = 'https://api.softwareavanzado.world/index.php?webserviceClient=administrator&webserviceVersion=1.0.0&option=contact&api=hal&filter[search]=201404082';

  private posturlapi = 'https://api.softwareavanzado.world/index.php?webserviceClient=administrator&webserviceVersion=1.0.0&option=contact&api=hal';
  
  constructor(

    private http:HttpClient,
    public router: Router

  ) { }


  getList() {
        
    //return this.http.get('https://api.softwareavanzado.world/index.php?webserviceClient=administrator&webserviceVersion=1.0.0&option=contact&api=hal&filter[search]=201408580');
    return this.http.get(this.urlapi);
    
  }

  AddContact(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.posturlapi, JSON.stringify(data))
      //this.http.post(this.apiUrlAdd, this.getFormData(data))
        .subscribe(res => {
          resolve(res);
          console.log(res);
          this.router.navigate(['/home']);
        }, (err) => {
          reject(err);
        });
    });
  }

  

}
