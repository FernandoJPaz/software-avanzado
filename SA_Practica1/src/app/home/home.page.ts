import { Component } from '@angular/core';
import { UserserviceService } from './../services/userservice.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  users: any[] = [];
  contacts:any[];
  usersx: any;
  
  
  constructor(
    private UserserviceService: UserserviceService

  ) {
    
  }

  ionViewDidLoad(){
    this.UserserviceService.getList()
    .subscribe(
      (data) => { // Success

        
        this.usersx = data['_embedded'];
        this.contacts = this.usersx['item']
        console.log(this.contacts);
        
      },
      (error) =>{
        console.error(error);
      }
    )
  }


}
